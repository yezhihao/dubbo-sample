package com.ibm.member.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ibm.member.dao.BaseDao;
import com.ibm.member.model.MemberBO;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private BaseDao dao;

    @RequestMapping("/login")
    public ModelAndView login(MemberBO user, HttpSession session) {
        MemberBO MemberBO = dao.get(user);

        ModelAndView mv = new ModelAndView();

        if (MemberBO == null) {
            mv.setViewName("redirect:/index.jsp");
            return mv;
        }
        ModelMap map = new ModelMap();
        session.setAttribute("USER", MemberBO);
        map.put("USER", user);
        mv.setViewName("redirect:/home.jsp");
        mv.addObject(MemberBO);
        return mv;
    }
}
