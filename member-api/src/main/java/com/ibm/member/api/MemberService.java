package com.ibm.member.api;

import java.util.List;

import com.ibm.member.common.exception.MemberException;
import com.ibm.member.model.MemberVO;

public interface MemberService {

    /** CUS-01-01 创建个人客户 */
    MemberVO registerMember(MemberVO vo) throws MemberException;

    List<MemberVO> findMember(MemberVO member);

}