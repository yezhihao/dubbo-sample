package com.ibm.member.common.enums;

public enum ResultCode {

    K0000("00000", "成功"), //

    K1000("K1000", "输入参数错误"), //
    K1001("K1001", "用户名不能为空"), //

    K2000("K2000", "找不到该用户"), //
    K2001("K2001", ""), //

    K8000("K8000", "调用远程系统失败"), //
    K9000("K9000", "操作失败");

    public final String code;

    public final String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

}