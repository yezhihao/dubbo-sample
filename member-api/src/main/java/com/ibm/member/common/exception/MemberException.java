package com.ibm.member.common.exception;

import com.ibm.member.common.enums.ResultCode;

public class MemberException extends Exception {

    private static final long serialVersionUID = 1L;

    private String code;

    public MemberException(ResultCode resultCode) {
        super(resultCode.message);
        this.code = resultCode.code;
    }

    public MemberException(ResultCode resultCode, String message) {
        super(resultCode.message + message);
        this.code = resultCode.code;
    }

    public MemberException(Throwable e) {
        super(e);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("错误代码:").append(this.code);
        sb.append(",错误信息:").append(super.getMessage());
        return sb.toString();
    }
}