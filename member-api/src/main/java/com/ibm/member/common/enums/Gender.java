package com.ibm.member.common.enums;

/** 性别 */
public enum Gender {

    /** 男性 */
    MALE("1"), //
    /** 女性 */
    FEMALE("2");

    public final String code;

    Gender(String code) {
        this.code = code;
    }

    public String toString() {
        return code;
    }

    public static Gender getEnum(String code) {
        for (Gender e : Gender.values())
            if (e.code.equals(code))
                return e;
        return null;
    }
}