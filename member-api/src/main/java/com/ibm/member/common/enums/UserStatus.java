package com.ibm.member.common.enums;

/** 会员状态 */
public enum UserStatus {

    /** 正常 */
    NORMAL("1"), //
    /** 注销 */
    CANCELLED("2"), //
    /** 锁定 */
    LOCKED("3");

    private final String code;

    UserStatus(String code) {
        this.code = code;
    }

    public String toString() {
        return code;
    }

    public String code() {
        return code;
    }

    public static UserStatus getEnum(String code) {
        for (UserStatus e : UserStatus.values())
            if (e.code.equals(code))
                return e;
        return null;
    }
}