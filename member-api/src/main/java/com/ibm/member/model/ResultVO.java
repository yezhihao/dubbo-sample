package com.ibm.member.model;

import com.ibm.member.common.enums.ResultCode;

public class ResultVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private String code;

    private String message;

    public ResultVO(ResultCode resultCode) {
        this.code = resultCode.code;
        this.message = resultCode.message;
    }

    public ResultVO(ResultCode resultCode, String message) {
        this.code = resultCode.code;
        this.message = resultCode.message + message;
    }

    public ResultVO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return ResultCode.K0000.code.equals(code);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("响应码:").append(this.code);
        sb.append(",描述:").append(this.message);
        return sb.toString();
    }

}