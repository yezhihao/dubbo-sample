package com.ibm.member.model;

import com.ibm.member.common.enums.UserStatus;
import com.ibm.member.common.enums.Gender;

public class MemberVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /** 客户编号 */
    private String custNo;

    /** 客户姓名 */
    private String custName;

    /** 姓名拼音 */
    private String nameSpell;

    /** 性别 */
    private Gender sexual;

    /** 出生日期 */
    private String birthday;

    /** 邮政编码 */
    private String postCode;

    /** 国籍 */
    private String nationality;

    /** 所属客户群 */
    private String memGroup;

    /** 用户名 */
    private String userName;

    /** 手机号码 */
    private String mobileNum;

    /** 手机所属地域 */
    private Integer mobileArea;

    /** 证件类型 */
    private String certType;

    /** 证件编号 */
    private String certNo;

    /** 客户状态 */
    private UserStatus status;

    /** 推荐人 */
    private String referee;

    /** 客户邮箱 */
    private String email;

    public MemberVO() {
    }

    public MemberVO(String custNo) {
        this.custNo = custNo;
    }

    public MemberVO(String certType, String certNo) {
        this.certType = certType;
        this.certNo = certNo;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getNameSpell() {
        return nameSpell;
    }

    public void setNameSpell(String nameSpell) {
        this.nameSpell = nameSpell;
    }

    public Gender getSexual() {
        return sexual;
    }

    public void setSexual(Gender sexual) {
        this.sexual = sexual;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMemGroup() {
        return memGroup;
    }

    public void setMemGroup(String memGroup) {
        this.memGroup = memGroup;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public Integer getMobileArea() {
        return mobileArea;
    }

    public void setMobileArea(Integer mobileArea) {
        this.mobileArea = mobileArea;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public String getReferee() {
        return referee;
    }

    public void setReferee(String referee) {
        this.referee = referee;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}