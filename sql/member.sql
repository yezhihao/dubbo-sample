SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for institution_base
-- ----------------------------
DROP TABLE IF EXISTS `institution_base`;
CREATE TABLE `institution_base` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `INSTITUTION_ID` varchar(30) NOT NULL COMMENT '机构客户编号',
  `INSTITUTION_NAME` varchar(60) NOT NULL COMMENT '机构客户名称',
  `INSTITUTION_ABBREVIATION` varchar(20) DEFAULT NULL COMMENT '机构客户规范化简称',
  `CERT_TYPE` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `CERT_NO` varchar(18) DEFAULT NULL COMMENT '证件编号',
  `CERT_DUE_DATE` char(8) DEFAULT NULL COMMENT '证件到期日',
  `INSTITUTION_TYPE` varchar(20) DEFAULT NULL COMMENT '机构类型',
  `ORG_PERMISSION_NUM` varchar(20) DEFAULT NULL COMMENT '开户许可证号',
  `CREDIT_CERTI_CODE` varchar(20) DEFAULT NULL COMMENT '信用代码号',
  `STATE_TAX_CODE` varchar(20) DEFAULT NULL COMMENT '国税登记证号',
  `LOCAL_TAX_CODE` varchar(20) DEFAULT NULL COMMENT '地税登记证件号',
  `USER_REVIEW_LEVEL` char(1) DEFAULT NULL COMMENT '用户审批等级',
  `REGISTER_CAPITAL` varchar(12) DEFAULT NULL COMMENT '注册资金',
  `OPERATE_SCALE` varchar(10) DEFAULT NULL COMMENT '经营规模',
  `OPERATE_SCOPE` varchar(60) DEFAULT NULL COMMENT '经营范围',
  `ORG_FAX_CODE` varchar(20) DEFAULT NULL COMMENT '机构传真号码',
  `OFFICIAL_ADD` varchar(100) DEFAULT NULL COMMENT '联系行政地址',
  `DETAIL_ADD` varchar(100) DEFAULT NULL COMMENT '联系详细地址',
  `POSTCODE` char(6) DEFAULT NULL COMMENT '邮政编码',
  `CLIENT_MGR` varchar(60) DEFAULT NULL COMMENT ' 所属客户经理',
  `STATUS` varchar(20) NOT NULL COMMENT '客户状态',
  `ORG_OWNER_NAME` varchar(20) DEFAULT NULL COMMENT '机构负责人姓名 ',
  `ORG_OWNER_CELL_PHONENO` varchar(20) DEFAULT NULL COMMENT '机构负责人手机号码 ',
  `ORG_OWNER_TYPE` varchar(2) DEFAULT NULL COMMENT '机构负责人类型 ',
  `ORG_OWNER_CERT_TYPE` varchar(2) DEFAULT NULL COMMENT '机构负责人证件类型 ',
  `ORG_OWNER_CERT_NO` varchar(20) DEFAULT NULL COMMENT '机构负责人证件号码 ',
  `ORG_OWNER_CERT_DUEDT`char(8) DEFAULT NULL COMMENT '机构负责人证件有效期 ',
  `HDPER_NAME` varchar(20) DEFAULT NULL COMMENT '经办人姓名 ',
  `HDPER_CELL_PHONENO` varchar(20) DEFAULT NULL COMMENT '经办人手机号码 ',
  `HDPER_CERT_TYPE` varchar(2) DEFAULT NULL COMMENT '经办人证件类型 ',
  `HDPER_CERT_NO` varchar(20) DEFAULT NULL COMMENT '经办人证件号码 ',
  `HDPER_CERT_DUE_DATE`char(8) DEFAULT NULL COMMENT '经办人证件有效期 ',
  `EMAIL` varchar(150) DEFAULT NULL COMMENT '电子邮箱 ',
  `WORK_PHONE` varchar(20) DEFAULT NULL COMMENT '单位电话 ',
  `WALLET_LEVEL`char(2) DEFAULT NULL COMMENT '钱包业务流程级别编号',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`SYS_ID`),
  CONSTRAINT INSTITUTION_ID UNIQUE (`INSTITUTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for institution_contact
-- ----------------------------
DROP TABLE IF EXISTS `institution_contact`;
CREATE TABLE `institution_contact` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `INSTITUTION_ID` varchar(20) NOT NULL COMMENT '机构客户ID',
  `CONTACT_TYPE` varchar(2) NOT NULL COMMENT '联系人类型 1 机构负责人 2 开户经办人',
  `RESPONSOR_TYPE` varchar(2) NOT NULL COMMENT '机构负责人类型 当联系人类型为1时必输 1 法定达标人 2 单位负责人',
  `CONTACT_NAME` varchar(30) NOT NULL COMMENT '联系人姓名',
  `CERT_TYPE` varchar(2) NOT NULL COMMENT '证件类型',
  `CERT_NO` varchar(18) NOT NULL COMMENT '证件号码',
  `CERT_DUE_DATE` char(8) NOT NULL COMMENT '证件到期日',
  `CONTACT_PHONE` varchar(20) DEFAULT NULL COMMENT '联络电话',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机',
  `EMAIL` varchar(30) DEFAULT NULL COMMENT 'Email',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for institution_operator
-- ----------------------------
DROP TABLE IF EXISTS `institution_operator`;
CREATE TABLE `institution_operator` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `INSTITUTION_ID` varchar(30) NOT NULL COMMENT '机构客户编号',
  `OPT_ID` varchar(12) NOT NULL COMMENT '操作员编号',
  `USER_ID` varchar(30) NOT NULL COMMENT '账号编号',
  `OPT_NAME` varchar(20) NOT NULL COMMENT '操作员姓名',
  `CERT_TYPE` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `CERT_NO` varchar(18) DEFAULT NULL COMMENT '证件编号',
  `CERT_DUE_DATE` char(8) DEFAULT NULL COMMENT '证件有效期',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `CONTRACT_WAY` varchar(100) DEFAULT NULL COMMENT '其它联络方式',
  `STATUS` varchar(20) DEFAULT NULL COMMENT '状态',
  `OPT_ROLE_CODE` varchar(30) DEFAULT NULL COMMENT '操作员类型代码',
  `OPT_ROLE_NAME` varchar(100) DEFAULT NULL COMMENT '操作员类型说明',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`SYS_ID`),
  CONSTRAINT OPT_ID UNIQUE (`OPT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for institution_register
-- ----------------------------
DROP TABLE IF EXISTS `institution_register`;
CREATE TABLE `institution_register` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `REGISTER_ID` varchar(30) DEFAULT NULL COMMENT ' 登记机构客户编号',
  `INSTITUTION_NAME` varchar(60) DEFAULT NULL COMMENT '机构客户全称',
  `ACCOUNT_BANK` varchar(50) DEFAULT NULL COMMENT '机构客户开户银行',
  `BANK_PROVINCE` varchar(20) DEFAULT NULL COMMENT '机构客户开户省份',
  `BANK_AREA` varchar(20) DEFAULT NULL COMMENT '机构客户开户地区',
  `LIAISONS_NAME` varchar(20) DEFAULT NULL COMMENT '业务联络人姓名',
  `LIAISON_MOBILE` varchar(20) DEFAULT NULL COMMENT ' 业务联络人手机号码',
  `COOPERATION_MSG` varchar(256) DEFAULT NULL COMMENT '合作留言',
  `REGISTER_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '机构客户登记时',
  `STATUS` varchar(20) DEFAULT NULL COMMENT '客户状态',
  PRIMARY KEY (`SYS_ID`),
  CONSTRAINT REGISTER_ID UNIQUE (`REGISTER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for institution_trace
-- ----------------------------
DROP TABLE IF EXISTS `institution_trace`;
CREATE TABLE `institution_trace` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `REGISTER_ID` varchar(30) NOT NULL COMMENT '登记机构客户ID',
  `TRACE_STATUS` varchar(3) NOT NULL COMMENT '跟进状态',
  `TRACE_RESULT` varchar(256) NOT NULL COMMENT '跟进结果 文本 营销备注',
  `TRACE_STAFF` varchar(60) DEFAULT NULL COMMENT '跟进人员',
  `TRACE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '跟进时间',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_address
-- ----------------------------
DROP TABLE IF EXISTS `member_address`;
CREATE TABLE `member_address` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `ADDRESS_ID` varchar(30) DEFAULT NULL COMMENT '地址编号',
  `ADD_NAME` varchar(100) NOT NULL COMMENT '地址名称',
  `OFFICIAL_ADD` varchar(100) DEFAULT NULL COMMENT '客户行政地址',
  `DETAIL_ADD` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `IS_PRIMARY_ADD` char(1) DEFAULT NULL COMMENT '是否是主地址',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `STATUS` varchar(20) NOT NULL COMMENT '地址状态',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_base
-- ----------------------------
DROP TABLE IF EXISTS `member_base`;
CREATE TABLE `member_base` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `USER_ID` bigint(20) DEFAULT NULL COMMENT '账户编号(对应系统认证)',
  `CHANNEL_NO` varchar(20) DEFAULT NULL COMMENT '渠道代码',
  `CHANNEL_NAME` varchar(100) DEFAULT NULL COMMENT '渠道描述',
  `IMPORT_CHANNEL_NO` varchar(20) DEFAULT NULL COMMENT '导流机构代码',
  `IMPORT_CHANNEL_NAME` varchar(60) DEFAULT NULL COMMENT '导流机构名称',
  `DEVICE_ID` varchar(20) DEFAULT NULL COMMENT '设备编号',
  `DEVICEL_NAME` varchar(60) DEFAULT NULL COMMENT '设备名称',
  `CUST_NAME` varchar(60) DEFAULT NULL COMMENT '客户姓名',
  `NAME_SPELL` varchar(60) DEFAULT NULL COMMENT '姓名拼音',
  `SEXUAL` char(1) DEFAULT NULL COMMENT '性别',
  `BIRTHDAY` char(8) DEFAULT NULL COMMENT '出生日期',
  `NATIONALITY` varchar(20) DEFAULT NULL COMMENT '国籍',
  `POST_CODE` varchar(20) DEFAULT NULL COMMENT '邮政编码',
  `MEM_GROUP` varchar(50) DEFAULT NULL COMMENT '所属客户群',
  `USER_NAME` varchar(20) DEFAULT NULL COMMENT '用户名',
  `NAME_STATUS` char(1) DEFAULT NULL COMMENT '用户名唯一性验证状态',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `MOBILE_AREA` smallint(6) DEFAULT NULL COMMENT '手机所属地域',
  `MOBILE_STATUS` char(1) DEFAULT NULL COMMENT '手机唯一性验证状态',
  `CERT_TYPE` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `CERT_NO` varchar(18) DEFAULT NULL COMMENT '证件编号',
  `CERT_STATUS` char(1) DEFAULT NULL COMMENT '证件唯一性验证',
  `CERT_DUE_DATE` char(8) DEFAULT NULL COMMENT '证件有效期',
  `IDENTITY_STATUS` char(1) DEFAULT NULL COMMENT '身份验证状态',
  `BRANCH_CODE` varchar(5) DEFAULT NULL COMMENT '所属分行代码',
  `BRANCH_NAME` varchar(60) DEFAULT NULL COMMENT '所属分行名称',
  `ORG_CODE` varchar(5) DEFAULT NULL COMMENT '所属机构代码',
  `ORG_NAME` varchar(60) DEFAULT NULL COMMENT ' 所属机构名称',
  `STATUS` varchar(20) DEFAULT NULL COMMENT '客户状态',
  `EMAIL` varchar(30) DEFAULT NULL COMMENT '客户邮箱',
  `EMAIL_STATUS` char(1) DEFAULT NULL COMMENT '邮箱唯一性验证',
  `CLIENT_MGR` varchar(60) DEFAULT NULL COMMENT ' 所属客户经理',
  `REFEREE` varchar(60) DEFAULT NULL COMMENT '推荐人',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `IDENTITY_PHOTO_ID` varchar(100) DEFAULT NULL COMMENT '证件影像地址',
  `SAFETY_LEVEL` tinyint DEFAULT NULL COMMENT '用户安全等级',
  PRIMARY KEY (`SYS_ID`),
  CONSTRAINT CUST_NO UNIQUE (`CUST_NO`),
  CONSTRAINT USER_NAME UNIQUE (`USER_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_contact
-- ----------------------------
DROP TABLE IF EXISTS `member_contact`;
CREATE TABLE `member_contact` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `CONTACT_ID` varchar(40) NOT NULL COMMENT '联系编号',
  `CONTACT_TOOL` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `TOOL_TYPE` varchar(50) DEFAULT NULL COMMENT '方式类型',
  `NUMBER` varchar(20) DEFAULT NULL COMMENT '号码明细',
  `IS_PREFERRED` char(1) DEFAULT NULL COMMENT '是否偏好联系方式',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `STATUS` varchar(20) NOT NULL COMMENT '联系方式状态',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_ext
-- ----------------------------
DROP TABLE IF EXISTS `member_ext`;
CREATE TABLE `member_ext` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) DEFAULT NULL COMMENT '客户编号',
  `CITY` varchar(10) DEFAULT NULL COMMENT '所在城市',
  `COMPANY` varchar(100) DEFAULT NULL COMMENT '工作单位',
  `POSITION` varchar(100) DEFAULT NULL COMMENT '工作岗位',
  `POST_LEVEL` varchar(100) DEFAULT NULL COMMENT '职称级别',
  `EDUCATED_LEVEL` varchar(10) DEFAULT NULL COMMENT '教育程度',
  `INDIVI_M_INCOME` decimal(18,2) DEFAULT NULL COMMENT '个人月收入',
  `FAMILY_M_INCOME` decimal(18,2) DEFAULT NULL COMMENT '家庭月收入',
  `MARITAL` varchar(100) DEFAULT NULL COMMENT '婚姻状况',
  `HOBBY` varchar(256) DEFAULT NULL COMMENT ' 兴趣爱好',
  `CONTACT_TIME` varchar(100) DEFAULT NULL COMMENT '最佳联系时间',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `HOME_ADD` varchar(100) DEFAULT NULL COMMENT '家庭地址',
  `COMP_TELEPHONE` varchar(20) DEFAULT NULL COMMENT '单位电话',
  `COMP_ADD` varchar(100) DEFAULT NULL COMMENT '单位地址',
  `HOME_TELEPHONE` varchar(20) DEFAULT NULL COMMENT '家庭电话',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_im
-- ----------------------------
DROP TABLE IF EXISTS `member_im`;
CREATE TABLE `member_im` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) DEFAULT NULL COMMENT '客户编号',
  `IM_TYPE` varchar(20) DEFAULT NULL COMMENT '即时通讯类型',
  `IM_NUM` varchar(40) DEFAULT NULL COMMENT '即时通讯号码',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_invoice
-- ----------------------------
DROP TABLE IF EXISTS `member_invoice`;
CREATE TABLE `member_invoice` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `INVOICE_ID` varchar(40) DEFAULT NULL COMMENT '发票编号',
  `INVOICE_TYPE` varchar(12) NOT NULL COMMENT '发票类型',
  `INVOICE_NAME` varchar(60) NOT NULL COMMENT '发票名称',
  `INVOICE_DETAIL` varchar(100) DEFAULT NULL COMMENT '发票信息',
  `IS_PRIMARY_INVOICE` char(1) DEFAULT NULL COMMENT '是否默认开具发票',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `STATUS` varchar(20) NOT NULL COMMENT '发票状态',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_login_code
-- ----------------------------
DROP TABLE IF EXISTS `member_login_code`;
CREATE TABLE `member_login_code` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `CHANNEL_NO` varchar(5) DEFAULT NULL COMMENT '开通渠道编号',
  `CHANNEL_NAME` varchar(60) DEFAULT NULL COMMENT '开通渠道名称',
  `DEVICE_ID` varchar(20) DEFAULT NULL COMMENT '登录设备编号',
  `DEVICEL_NAME` varchar(60) DEFAULT NULL COMMENT '登录设备名称',
  `OTHER_NAME` varchar(60) DEFAULT NULL COMMENT '第三方用户名',
  `USER_NAME` varchar(20) DEFAULT NULL COMMENT '用户名',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `CERT_NO` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `CARD_NO` varchar(32) DEFAULT NULL COMMENT '银行卡号',
  `REGISTER_CODE` varchar(128) DEFAULT NULL COMMENT '登陆密码',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `STATUS` smallint(6) NOT NULL COMMENT '登录状态',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_message
-- ----------------------------
DROP TABLE IF EXISTS `member_message`;
CREATE TABLE `member_message` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `MESSAGE_ID` varchar(30) DEFAULT NULL COMMENT '消息编号',
  `SEND_CUST_ID` varchar(30) DEFAULT NULL COMMENT '发送客户编号',
  `RECIEVE_CUST_ID` varchar(15) DEFAULT NULL COMMENT '接收客户编号',
  `SEND_APP` varchar(20) DEFAULT NULL COMMENT '消息发送应用代码',
  `MESSAGE_TITLE` varchar(100) DEFAULT NULL COMMENT '消息标题',
  `MESSAGE_CONTENT` varchar(1000) DEFAULT NULL COMMENT '消息内容',
  `MESSAGE_STATUS` char(2) DEFAULT NULL COMMENT '消息状态',
  `SEND_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '发送时间',
  `READ_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '阅读时间',
  `DELETE_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '删除时间',
  PRIMARY KEY (`SYS_ID`),
  CONSTRAINT MESSAGE_ID UNIQUE (`MESSAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_mobile
-- ----------------------------
DROP TABLE IF EXISTS `member_mobile`;
CREATE TABLE `member_mobile` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) DEFAULT NULL COMMENT '客户编号',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `MOBILE_STATUS` char(1) DEFAULT NULL COMMENT '是否主号',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_refree
-- ----------------------------
DROP TABLE IF EXISTS `member_refree`;
CREATE TABLE `member_refree` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) DEFAULT NULL COMMENT '客户编号',
  `REFREE_ID` varchar(30) DEFAULT NULL COMMENT '推荐人编号',
  `REFREE_METHOD` varchar(50) DEFAULT NULL COMMENT '推荐方式',
  `CHANNEL` varchar(50) DEFAULT NULL COMMENT '推荐渠道',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `STATUS` varchar(20) DEFAULT NULL COMMENT '关系状态',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for member_register_code_his
-- ----------------------------
DROP TABLE IF EXISTS `member_register_code_his`;
CREATE TABLE `member_register_code_his` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) DEFAULT NULL COMMENT '客户编号',
  `CHANNEL_NO` varchar(5) DEFAULT NULL COMMENT '登录渠道编号',
  `CHANNEL_NAME` varchar(60) DEFAULT NULL COMMENT '登录渠道名称',
  `DEVICE_ID` varchar(20) DEFAULT NULL COMMENT '登录设备编号',
  `DEVICEL_NAME` varchar(60) DEFAULT NULL COMMENT '登录设备名称',
  `ACCOUNT_TYPE` char(1) DEFAULT NULL COMMENT '账号类型',
  `INPUT_ACCOUNT` varchar(32) DEFAULT NULL COMMENT '输入账号',
  `INPUT_CODE` varchar(128) DEFAULT NULL COMMENT ' 输入密码',
  `MATCH_RESULT` char(1) DEFAULT NULL COMMENT '是否匹配成功',
  `OPERATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `safety_level_standard`;
CREATE TABLE `safety_level_standard` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUSTOMER_TYPE` TINYINT NOT NULL COMMENT '客户类型',
  `SAFETY_LEVEL` TINYINT DEFAULT NULL COMMENT '客户安全等级',
  `SAFETY_LEVEL_DEMAND` varchar(30) DEFAULT NULL COMMENT '升级前安全等级要求',
  `SAFETY_CONDITION` TINYINT DEFAULT NULL COMMENT '安全条件',
  `CLEAR_COUNT` bigint(10) DEFAULT NULL COMMENT '转入交易笔数',
  `TXN_TYPE` varchar(4) DEFAULT NULL COMMENT '交易类型',
  `TXN_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '交易历时时间',
  `SAFETY_SCORE` TINYINT DEFAULT NULL COMMENT '安全评分',
  `START_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '标准生效日期',
  `SPARE1` varchar(30) DEFAULT NULL COMMENT '备用字段',
  `SPARE2` varchar(30) DEFAULT NULL COMMENT '备用字段',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `safety_level_change_record`;
CREATE TABLE safety_level_change_record (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `CUST_NAME` varchar(60) DEFAULT NULL COMMENT '客户姓名',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `CERT_NO` varchar(18) DEFAULT NULL COMMENT '证件编号',
  `BANK_NO` varchar(5) DEFAULT NULL COMMENT '联网行编号',
  `BEFORE_LEVEL` tinyint DEFAULT NULL COMMENT '变更前等级',
  `AFTER_LEVEL` tinyint DEFAULT NULL COMMENT '变更后等级',
  `REASON` varchar(30) DEFAULT NULL COMMENT '变更原因',
  `SOURCE` varchar(20) DEFAULT NULL COMMENT '来源',
  `CREATE_TIME` timestamp DEFAULT CURRENT_TIMESTAMP COMMENT '变更时间',
  PRIMARY KEY (`SYS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Create Index
-- ----------------------------
CREATE INDEX `institutionBase_certType` ON `institution_base`(`CERT_TYPE`);
CREATE INDEX `institutionBase_certNo` ON `institution_base`(`CERT_NO`);
CREATE INDEX `institutionBase_institutionType` ON `institution_base`(`INSTITUTION_TYPE`);
CREATE INDEX `institutionBase_status` ON `institution_base`(`STATUS`);
CREATE INDEX `institutionBase_createTime` ON `institution_base`(`CREATE_TIME`);

CREATE INDEX `institutionOperator_institutionId` ON `institution_operator`(`INSTITUTION_ID`);
CREATE INDEX `institutionOperator_userId` ON `institution_operator`(`USER_ID`);
CREATE INDEX `institutionOperator_certType` ON `institution_operator`(`CERT_TYPE`);
CREATE INDEX `institutionOperator_certNo` ON `institution_operator`(`CERT_NO`);
CREATE INDEX `institutionOperator_mobileNum` ON `institution_operator`(`MOBILE_NUM`);
CREATE INDEX `institutionOperator_status` ON `institution_operator`(`STATUS`);
CREATE INDEX `institutionOperator_optRoleCode` ON `institution_operator`(`OPT_ROLE_CODE`);
CREATE INDEX `institutionOperator_createTime` ON `institution_operator`(`CREATE_TIME`);

CREATE INDEX `institutionTrace_registerId` ON `institution_trace`(`REGISTER_ID`);
CREATE INDEX `institutionTrace_traceStatus` ON `institution_trace`(`TRACE_STATUS`);
CREATE INDEX `institutionTrace_traceStaff` ON `institution_trace`(`TRACE_STAFF`);
CREATE INDEX `institutionTrace_traceTime` ON `institution_trace`(`TRACE_TIME`);

CREATE INDEX `memberBase_userId` ON `member_base`(`USER_ID`);
CREATE INDEX `memberBase_channelNo` ON `member_base`(`CHANNEL_NO`);
CREATE INDEX `memberBase_importChannelNo` ON `member_base`(`IMPORT_CHANNEL_NO`);
CREATE INDEX `memberBase_sexual` ON `member_base`(`SEXUAL`);
CREATE INDEX `memberBase_birthday` ON `member_base`(`BIRTHDAY`);
CREATE INDEX `memberBase_memGroup` ON `member_base`(`MEM_GROUP`);
CREATE INDEX `memberBase_status` ON `member_base`(`STATUS`);
CREATE INDEX `memberBase_safetyLevel` ON `member_base`(`SAFETY_LEVEL`);

CREATE INDEX `memberMessage_sendCustId` ON `member_message`(`SEND_CUST_ID`);
CREATE INDEX `memberMessage_recieveCustId` ON `member_message`(`RECIEVE_CUST_ID`);
CREATE INDEX `memberMessage_messageStatus` ON `member_message`(`MESSAGE_STATUS`);

CREATE INDEX `safetyLevelChangeRecord_custNo` ON `safety_level_change_record`(`CUST_NO`);
CREATE INDEX `safetyLevelChangeRecord_certNo` ON `safety_level_change_record`(`CERT_NO`);
CREATE INDEX `safetyLevelChangeRecord_certNo` ON `safety_level_change_record`(`SOURCE`);