DROP TABLE IF EXISTS `member_base`;
CREATE TABLE `member_base` (
  `SYS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CUST_NO` varchar(20) NOT NULL COMMENT '客户编号',
  `CUST_NAME` varchar(60) DEFAULT NULL COMMENT '客户姓名',
  `NAME_SPELL` varchar(60) DEFAULT NULL COMMENT '姓名拼音',
  `SEXUAL` char(1) DEFAULT NULL COMMENT '性别',
  `BIRTHDAY` char(8) DEFAULT NULL COMMENT '出生日期',
  `NATIONALITY` varchar(20) DEFAULT NULL COMMENT '国籍',
  `POST_CODE` varchar(20) DEFAULT NULL COMMENT '邮政编码',
  `MEM_GROUP` varchar(50) DEFAULT NULL COMMENT '所属客户群',
  `USER_NAME` varchar(20) DEFAULT NULL COMMENT '用户名',
  `MOBILE_NUM` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `MOBILE_AREA` smallint(6) DEFAULT NULL COMMENT '手机所属地域',
  `CERT_TYPE` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `CERT_NO` varchar(18) DEFAULT NULL COMMENT '证件编号',
  `STATUS` varchar(20) DEFAULT NULL COMMENT '客户状态',
  `EMAIL` varchar(30) DEFAULT NULL COMMENT '客户邮箱',
  `REFEREE` varchar(60) DEFAULT NULL COMMENT '推荐人',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `REVISE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`SYS_ID`),
  CONSTRAINT USER_NAME UNIQUE (`USER_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-----------------------------Delete
drop table activity;
drop table comment;
drop table mark;
drop table photo;
drop table album;
drop table user;
create sequence seq_photo;
create sequence seq_album;
create sequence seq_comment;
create sequence seq_actvity;
create sequence seq_mark;
-----------------------------Insert
insert into user values('a','大道无形','a','a',null,1);
commit;
-----------------------------Search
select * from user;
select * from album;
select * from photo;
select * from comment;
select * from activity;

select * from photo where album_id = 1;
select * from comment where photo_id = 1;
select * from activity where photo_id = 1;
select * from photo where album_id = 1;


delete activity where photo_id = 1;
delete comment where photo_id = 1;


delete photo where album_id = 1;
delete album where album_id = 1;





