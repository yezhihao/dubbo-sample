package com;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.dubbo.config.annotation.Reference;
import com.ibm.member.api.MemberService;
import com.ibm.member.common.enums.UserStatus;
import com.ibm.member.common.enums.Gender;
import com.ibm.member.model.MemberVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:Consumer.xml")
public class TestService {

    @Reference(check = false)
    private MemberService memberService;

    @Test
    public void getMember() {
        memberService.findMember(null);
    }

    public static void main(String[] args) {
        MemberVO vo = new MemberVO();
        vo.setCustNo("123123");
        vo.setCustName("阿萨德阿萨德");
        vo.setNameSpell("asdasdasd");
        vo.setSexual(Gender.MALE);
        vo.setBirthday("123123123");
        vo.setPostCode("123123123");
        vo.setNationality("咋死的");
        vo.setMemGroup("asdasd1");
        vo.setUserName("asdasdasdsd");
        vo.setMobileNum("123123123123");
        vo.setMobileArea(213);
        vo.setCertType("123123");
        vo.setCertNo("132123123123");
        vo.setStatus(UserStatus.NORMAL);
        vo.setReferee("123");
        vo.setEmail("asdasdasd@123123.123");
    }

}