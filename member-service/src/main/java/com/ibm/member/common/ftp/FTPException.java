package com.ibm.member.common.ftp;

public class FTPException extends Exception {

    private static final long serialVersionUID = 1L;

    public FTPException() {
        super();
    }

    public FTPException(String message, Throwable cause) {
        super(message, cause);
    }

    public FTPException(String message) {
        super(message);
    }

}