package com.ibm.member.common.util;

import java.util.Random;

public class RandomUtil {

    private static final String ALPHANUMERIC = "_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String ALPHABETIC = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        while (true) {
            System.out.println(randomString(12));
        }
    }

    /** 返回一个定长的随机字符串(只包含大小写字母、数字、下划线,首字母不能为数字) */
    public static String randomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        sb.append(ALPHABETIC.charAt(RANDOM.nextInt(ALPHABETIC.length())));
        for (int i = 1; i < length; i++)
            sb.append(ALPHANUMERIC.charAt(RANDOM.nextInt(ALPHANUMERIC.length())));
        return sb.toString();
    }

}