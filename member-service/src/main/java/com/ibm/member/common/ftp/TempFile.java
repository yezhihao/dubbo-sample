package com.ibm.member.common.ftp;

import java.io.File;
import java.net.URI;

/** 临时文件,对象销毁时删除本地文件 */
public class TempFile extends File {

    private static final long serialVersionUID = 301077366599181567L;

    public TempFile(String pathname) {
        super(pathname);
    }

    public TempFile(String parent, String child) {
        super(parent, child);
    }

    public TempFile(File parent, String child) {
        super(parent, child);
    }

    public TempFile(URI uri) {
        super(uri);
    }

    @Override
    protected void finalize() throws Throwable {
        this.delete();
        super.finalize();
    }

}