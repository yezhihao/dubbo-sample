package com.ibm.member.common.ftp;

public abstract class AbstractFTPClient implements FTPClient {

    /** FTP配置信息 */
    protected FTPConfig config;

    /** 创建FTP实现类后,进行FTP连接 */
    public AbstractFTPClient(FTPConfig config) throws FTPException {
        this.config = config;
        connect();
    }

    /** 销毁FTP对象前,关闭FTP连接 */
    @Override
    protected void finalize() throws Throwable {
        disconnect();
        super.finalize();
    }

    /** 连接FTP服务器 */
    public abstract void connect() throws FTPException;

    /** 关闭FTP服务器 */
    public abstract void disconnect();

}
