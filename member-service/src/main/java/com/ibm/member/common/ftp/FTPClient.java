package com.ibm.member.common.ftp;

import java.io.File;

public interface FTPClient {

    void changeDir(String directory) throws FTPException;

    void upload(File file, String fileName) throws FTPException;

    void upload(byte[] bytes, String fileName) throws FTPException;

    File download(String ftpFileName, String localFileName) throws FTPException;

    void download(String ftpFileName, File localFile) throws FTPException;

    void delete(String fileName) throws FTPException;

    void rename(String oldFileName, String newFileName) throws FTPException;

}