package com.ibm.member.common.util;

import java.lang.reflect.Method;

import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.ibm.member.model.MemberBO;

public class GeneratorSQL {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void main(String[] args) {
        Class clazz = MemberBO.class;
        Table table = (Table) clazz.getAnnotation(Table.class);
        Method[] methods = clazz.getDeclaredMethods();

        StringBuilder select = new StringBuilder("select \n");
        StringBuilder where = new StringBuilder();

        for (Method method : methods) {
            String name = method.getName();
            if (name.startsWith("get") && !name.equals("getClass")) {
                Column meta = method.getAnnotation(Column.class);
                if (meta == null) {
                    System.out.println(name);
                    return;
                }
                String tname = meta.name();
                name = StringUtils.uncapitalize(name.substring(3, name.length()));

                select.append(tname).append(",\n");

                where.append("<if test=\"").append(name).append(" != null\">\n");
                where.append("\tand ").append(tname).append("=#{").append(name).append("}\n");
                where.append("</if>\n");
            }
        }
        select.deleteCharAt(select.length() - 2);
        select.append("from ").append(table.name()).append("\n");
        select.append("<where>\n").append(where).append("</where>");
        System.out.println(select.toString());
    }
}