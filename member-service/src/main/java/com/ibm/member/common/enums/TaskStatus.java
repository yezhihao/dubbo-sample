package com.ibm.member.common.enums;

public enum TaskStatus {

    /** 失败 */
    FAILURE("0"), //
    /** 成功 */
    SUCCESS("1"), //
    /** 执行 */
    PENDING("2"),
    /** 执行 */
    FINISH("3");

    private final String code;

    TaskStatus(String code) {
        this.code = code;
    }

    public String toString() {
        return code;
    }

    public String code() {
        return code;
    }

    public static TaskStatus getEnum(String code) {
        for (TaskStatus e : TaskStatus.values())
            if (e.code.equals(code))
                return e;
        return null;
    }
}
