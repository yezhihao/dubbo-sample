package com.ibm.member.common.util;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;

public class ValidateUtil {

    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /** 根据VO中注解 校验指定分组的数据 */
    public static <T> String validateModel(Object obj, Class<T> group) {
        StringBuilder message = new StringBuilder(16);

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj, group);
        for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
            message.append(constraintViolation.getMessage());
        }

        String msg = message.toString();
        if (StringUtils.isBlank(msg))
            return null;
        return msg;
    }

    /** 根据VO中注解 校验数据 */
    public static String validateModel(Object obj) {
        StringBuilder message = new StringBuilder(16);

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);
        for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
            message.append(constraintViolation.getMessage());
        }

        String msg = message.toString();
        if (StringUtils.isBlank(msg))
            return null;
        return msg;
    }

}