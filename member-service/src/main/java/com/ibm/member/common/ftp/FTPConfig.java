package com.ibm.member.common.ftp;

import java.util.Properties;

/** 本类封装了ftp上送文件的配置参数 */
public class FTPConfig {

    public FTPClient getInstance() throws FTPException {
        if (Boolean.TRUE == sftp)
            return new SFTPClientImpl(this);
        return new FTPClientImpl(this);
    }

    /** 自定义扩展信息 */
    private Properties properties;

    /** IP地址 */
    private String host;

    /** 端口 */
    private Integer port = 21;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /** 连接超时 */
    private int timeout = 20000;

    /** 密钥路径 */
    private String keypath;

    /** 口令(密钥登录) */
    private String passphrase;

    /** 是否使用SFTP */
    private Boolean sftp;

    public Properties getProperties() {
        if (this.properties == null)
            this.properties = new Properties();
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getKeypath() {
        return keypath;
    }

    public void setKeypath(String keypath) {
        this.keypath = keypath;
    }

    public String getPassphrase() {
        return passphrase;
    }

    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    public Boolean getSftp() {
        return sftp;
    }

    public void setSftp(Boolean sftp) {
        this.sftp = sftp;
    }

}