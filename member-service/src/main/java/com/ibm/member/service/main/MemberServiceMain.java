package com.ibm.member.service.main;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MemberServiceMain {

    @SuppressWarnings("resource")
    public static void main(String[] args) throws IOException {
        System.out.println("****************** Begin Start Service ******************");

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:common-server-provider.xml");
        context.start();

        System.out.println("****************** Start Service Complete ******************");
        System.in.read();
    }
}
