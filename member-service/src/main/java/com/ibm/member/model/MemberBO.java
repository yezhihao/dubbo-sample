package com.ibm.member.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.ibm.member.common.enums.UserStatus;
import com.ibm.member.common.enums.Gender;

@Entity
@Table(name = "MEMBER_BASE")
public class MemberBO extends BaseBO {

    private static final long serialVersionUID = 1L;

    /** 客户编号 */
    private String custNo;

    /** 客户姓名 */
    private String custName;

    /** 姓名拼音 */
    private String nameSpell;

    /** 性别 */
    private Gender sexual;

    /** 出生日期 */
    private String birthday;

    /** 邮政编码 */
    private String postCode;

    /** 国籍 */
    private String nationality;

    /** 所属客户群 */
    private String memGroup;

    /** 用户名 */
    private String userName;

    /** 手机号码 */
    private String mobileNum;

    /** 手机所属地域 */
    private Integer mobileArea;

    /** 证件类型 */
    private String certType;

    /** 证件编号 */
    private String certNo;

    /** 客户状态 */
    private UserStatus status;

    /** 推荐人 */
    private String referee;

    /** 客户邮箱 */
    private String email;

    public MemberBO() {
    }

    public MemberBO(String custNo) {
        this.custNo = custNo;
    }

    public MemberBO(String certType, String certNo) {
        this.certType = certType;
        this.certNo = certNo;
    }

    @Column(name = "CUST_NO")
    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    @Column(name = "POST_CODE")
    @Length(max = 7, message = "邮政编码最大长度为7位")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "CUST_NAME")
    @Length(max = 32, message = "客户姓名最大长度为32位")
    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    @Column(name = "NAME_SPELL")
    public String getNameSpell() {
        return nameSpell;
    }

    public void setNameSpell(String nameSpell) {
        this.nameSpell = nameSpell;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "SEXUAL")
    public Gender getSexual() {
        return sexual;
    }

    public void setSexual(Gender sexual) {
        this.sexual = sexual;
    }

    @Column(name = "BIRTHDAY")
    @Length(max = 8, message = "出生日期最大长度为8位(4位年2位月2位日)")
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Column(name = "NATIONALITY")
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Column(name = "MEM_GROUP")
    public String getMemGroup() {
        return memGroup;
    }

    public void setMemGroup(String memGroup) {
        this.memGroup = memGroup;
    }

    @Column(name = "USER_NAME")
    @Length(max = 20, message = "用户名最大长度为20位")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "MOBILE_NUM")
    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    @Column(name = "MOBILE_AREA")
    public Integer getMobileArea() {
        return mobileArea;
    }

    public void setMobileArea(Integer mobileArea) {
        this.mobileArea = mobileArea;
    }

    @Column(name = "CERT_TYPE")
    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    @Column(name = "CERT_NO")
    @Length(max = 18, message = "证件编号最大长度为18位")
    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS")
    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    @Column(name = "REFEREE")
    @Length(max = 60, message = "推荐人最大长度为60位")
    public String getReferee() {
        return referee;
    }

    public void setReferee(String referee) {
        this.referee = referee;
    }

    @Column(name = "EMAIL")
    @Length(max = 30, message = "客户邮箱最大长度为30位")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}