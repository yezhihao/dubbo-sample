package com.ibm.member.model.comm;

import java.io.Serializable;
import java.util.List;

public class PaginationResult<R> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 结果集 */
    private List<R> r;

    /** 分页信息 */
    private Pagination pagination;

    public PaginationResult() {
    }

    public PaginationResult(List<R> r, Pagination pagination) {
        this.r = r;
        this.pagination = pagination;
    }

    public List<R> getR() {
        return r;
    }

    public void setR(List<R> r) {
        this.r = r;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}