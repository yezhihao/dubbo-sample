package com.ibm.member.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ibm.member.common.enums.TaskStatus;

@Entity
@Table(name = "ASYN_TASK")
public class AsynTaskBO extends BaseBO {

    private static final long serialVersionUID = 1L;

    private Date startTime;
    private Date endTime;
    private String clazz;
    private String param;
    private String result;

    // 剩余执行次数
    private Integer remain;

    // 调用频率(秒)
    private Integer frequency;

    private TaskStatus status;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getRemain() {
        return remain;
    }

    public void setRemain(Integer remain) {
        this.remain = remain;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

}
