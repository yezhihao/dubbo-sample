package com.ibm.member.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibm.member.common.enums.UserStatus;
import com.ibm.member.common.util.BeanUtil;
import com.ibm.member.dao.BaseDao;
import com.ibm.member.dao.MemberDao;
import com.ibm.member.model.MemberBO;
import com.ibm.member.model.MemberVO;

@Repository
public class MemberDaoImpl implements MemberDao {

    @Autowired
    private BaseDao dao;

    @Override
    public long create(MemberVO vo) {
        MemberBO bo = to(vo);
        return dao.create(bo);
    }

    @Override
    public void update(MemberVO vo) {
        MemberBO bo = to(vo);
        dao.update(bo);
    }

    @Override
    public List<MemberVO> find(MemberVO vo) {
        MemberBO param = to(vo);
        List<MemberBO> bos = dao.find(param);
        if (bos == null)
            return null;
        List<MemberVO> resultVO = new ArrayList<MemberVO>(bos.size());
        for (MemberBO bo : bos) {
            resultVO.add(to(bo));
        }
        return resultVO;
    }

    public MemberVO get(MemberVO vo) {
        MemberBO bo = to(vo);
        bo = dao.selectOne(bo);
        if (bo == null)
            return null;
        return to(bo);
    }

    public MemberVO get(String custNo) {
        MemberVO vo = new MemberVO();
        vo.setCustNo(custNo);
        return get(vo);
    }

    @Override
    public void delete(String custNo) {
        MemberVO vo = new MemberVO(custNo);
        vo = get(vo);
        if (vo != null) {
            vo.setStatus(UserStatus.CANCELLED);
            update(vo);
        }
    }

    private MemberVO to(MemberBO bo) {
        MemberVO vo = new MemberVO();
        BeanUtil.copyBean(bo, vo);
        return vo;
    }

    private MemberBO to(MemberVO vo) {
        MemberBO bo = new MemberBO();
        BeanUtil.copyBean(vo, bo);
        return bo;
    }

}