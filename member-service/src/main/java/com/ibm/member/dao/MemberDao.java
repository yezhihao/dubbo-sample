package com.ibm.member.dao;

import java.util.List;

import com.ibm.member.model.MemberVO;

public interface MemberDao {

    long create(MemberVO vo);

    void update(MemberVO vo);

    List<MemberVO> find(MemberVO vo);

    MemberVO get(MemberVO vo);

    MemberVO get(String custNo);

    void delete(String custNo);

}