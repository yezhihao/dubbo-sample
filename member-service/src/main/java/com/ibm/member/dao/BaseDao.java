package com.ibm.member.dao;

import java.util.List;

import com.ibm.member.model.BaseBO;
import com.ibm.member.model.comm.Pagination;
import com.ibm.member.model.comm.PaginationResult;

public interface BaseDao {

    long create(BaseBO bo);

    boolean merge(BaseBO newBo);

    void update(BaseBO newBo);

    void delete(BaseBO bo);

    <T extends BaseBO> T selectOne(BaseBO bo);

    <T extends BaseBO> T get(T bo);

    <T extends BaseBO> List<T> find(T bo);

    <T extends BaseBO> PaginationResult<T> find(T bo, Pagination pagination);

}